import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import { AmoModule } from './amo/amo.module';



@Module({
    imports: [AmoModule],
    controllers: [AppController],
    providers: [],
    exports: []
})
export class AppModule {}
