import {BadRequestException, Controller, Get, Param} from '@nestjs/common';
import {AmoService} from "./amo.service";
import {response} from "express";

@Controller('amo')
export class AmoController {
    constructor(private readonly amoService: AmoService) {
    }

    @Get('leads/:filter')
    getAListOfDealsForFilters(@Param('filter') filter: string | undefined): any {
        if ((filter !== undefined) && (filter !== '') && (filter.length < 3)) throw new BadRequestException('Фильтр должен быть длинне 2 символов');
        return this.amoService.getAListOfDeals(filter)
    }

    @Get('leads/')
    getAListOfDeals(): any {

        return this.amoService.getAListOfDeals(undefined)
    }
}
