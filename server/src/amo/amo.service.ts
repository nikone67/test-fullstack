import {Injectable} from '@nestjs/common';
import {HttpService} from "@nestjs/axios";
import {urlencoded} from "express";

@Injectable()
export class AmoService {
    constructor(private readonly httpService: HttpService) {

    }

    /**
     * токен для авторизации
     */
    authorizationCode: string = '';
    /**
     * Код авторизации из личного кабинета
     */
    code: string = `def502007a79dd1e8a7199a1eab3e519668c73d8ad8f864da033647618ca923380afc0b119040070fd90306ab48354b0aae637f321f21653b51b242c1167e8c072d27f5884891ee49523949f02ab1392ce82d590e0c12273cde315ea51d6d0d41c98d9dae6c050bdf7a8b1eedf31c88a5f0338a7b5f54341376ace7d21eadd599ed4f19485fdc202135c4df830ad912e12d5b445d75cce308e873f425f9a9c4051b7c34e0a57e8eba7769ae4977a89a57483585a91b4840acce71447f9de9afa043135e6472234e00ea6a3ad749550478eb2cbefdd47aa02c5bf27c1df47485834a07b62d53191273f5a4c75fbcdb15bc82acae0c61c491021a7c6c27601d6c8b6e000f22da15f062d8afd689fb0e2c4c660b956bb6dd0273ae22c1dc76de8db54e8597fb768a8a0d295a89384541b9a6b0973f52b4cb631143dd44ca75aa6dfdb9d4ceaeb855afcf710c6a8b4afb6d20886dc286b64020136063e731e1c118acb54beeb3d7ec7f19762a166dbdd6668d6939b7e005e6b471c07fbf650f0f154594544e9635540e81188e31aafc9cf150f09f7b1791c7eacc35eb8a233a0f58d419afc505a6d086297e412fe96510109347a21571d02f3af431cf6f640265119ecd283355a22b989b561cac5f32a957109a648314b69b46f61e6485aa40fd3f3a9e852790cc4c9`;
    /**
     * Список контактов
     */
    contacts: any = {};

    /**
     * Список пользователей
     */
    users: any = {};

    /**
     * Адрес для запросов
     */
    amourl: string = 'https://nikone67.amocrm.ru/';

    /**
     * получение списка сделок
     * @param filter
     */
    async getAListOfDeals(filter: string | undefined) {
        let resp: any = {};
        let error: any = undefined;
        try {
            if (this.authorizationCode === '') this.authorizationCode = await this.authorization();
            const authorizationCode: string = this.authorizationCode;
            let queryParams = '?with=contacts';
            if (filter !== undefined) queryParams =queryParams + `&query=${encodeURI(filter)}`

            let response: any = await this.httpService.axiosRef.get(this.amourl + '/api/v4/leads' + queryParams, {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + authorizationCode
                }
            })
            resp = response.data._embedded.leads;
            await this.getContacts(authorizationCode);

            for (let elem of resp) {
                if (this.users[elem.responsible_user_id] === undefined) this.users = await this.getUser(authorizationCode);
                elem.responsible_user_name = this.users[elem.responsible_user_id].name;
                let contacts: any = elem._embedded.contacts
                if (this.contacts[contacts[0].id] === undefined) this.contacts = await this.getContacts(authorizationCode)
                elem.contacts = this.contacts[contacts[0].id];

            }



        } catch (e) {
            console.log(e)
            error = e
        }
        if (error !== undefined) throw error
        return resp

    }

    /**
     * получение списка юзеров
     * @param authorizationCode
     */
    async getUser(authorizationCode: string) {
        let error: any;
        let resp: any = {};
        try {
            let response: any = await this.httpService.axiosRef.get(this.amourl + '/api/v4/users', {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + authorizationCode
                }
            })
            response = response.data._embedded.users;
            for (let elem of response) {
                resp[elem.id] = {
                    name: elem.name,
                    email: elem.email,
                }
            }

            // for (let elem of response) {
            //     console.log(elem)
            // }

        } catch (e) {
            error = e
        }

        if (error !== undefined) throw error
        return resp
    }

    /**
     * Метод для авторизации
     */
    async authorization() {
        let resp: string = '';
        let error: any = undefined;

        try {
            let response = await this.httpService.axiosRef.post(this.amourl + '/oauth2/access_token', {
                    "client_id": "d6479aa1-ca57-4f56-9636-6695f479fb30",
                    "client_secret": "o27L06Di2Mv0wJcMm3QunHKCcWrl18O5towkxzA7V0TJb4pJbeZ4Z6Js1BiRBVfz",
                    "grant_type": "authorization_code",
                    "code": this.code,
                    "redirect_uri": "https://www.google.ru/"

                }, {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }
            );
            let body: any = response.data;
            if (body.hasOwnProperty('access_token') && (typeof body.access_token === 'string')) {
                resp = body.access_token
            } else error = 'Не найден access_token'
        } catch (e) {

            error = e
        }

        if (error !== undefined) throw error
        return resp

    }

    /**
     * получение списка контактов
     * @param authorizationCode
     */
    async getContacts(authorizationCode: string) {
        let error: any;
        let resp: any = {};
        try {
            let response: any = await this.httpService.axiosRef.get(this.amourl + '/api/v4/contacts', {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + authorizationCode
                }
            })
            response = response.data._embedded.contacts;

            for (let elem of response) resp[elem.id] = elem;

        } catch (e) {
            error = e
        }

        if (error !== undefined) throw error
        return resp


    }
}
