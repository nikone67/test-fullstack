import { Module } from '@nestjs/common';
import {AmoController} from "./amo.controller";
import {AmoService} from "./amo.service";
import {HttpModule} from "@nestjs/axios";

@Module({
    imports: [HttpModule],
    controllers: [ AmoController],
    providers: [ AmoService],
})
export class AmoModule {}
